<?php 
	class post extends database{
		protected $title = NULL;
		public $id = NULL;
		protected $content = NULL;
		protected $image = NULL;
		protected $cate_id = NULL;
		protected $sub_id = NULL;
		protected $summary = NULL;
		protected $author = NULL;
		
		public function set_title($title){
			$this->title = $title;
		}
		public function set_id($id){
			$this->id = $id;
		}
		public function set_content($content){
			$this->content = $content;
		}
		public function set_image($image){
			$this->image = $image;
		}
		public function set_cate_id($cate_id){
			$this->cate_id = $cate_id;
		}
		public function set_sub_id($sub_id){
			$this->sub_id = $sub_id;
		}
		public function set_summary($summary){
			$this->summary = $summary;
		}
		public function set_author($author){
			$this->author = $author;	
		}
		
		public function add_post(){
			$sql = "SELECT title FROM post WHERE title='$this->title'";
			$this->query($sql);
			if($this->num_row() >0){
				return "fail";			
			}else{
				$sql = "INSERT INTO post(title,content,image,cate_id,sub_cate,summary,date_post,author) VALUES ('$this->title','$this->content','$this->image','$this->cate_id','$this->sub_id','$this->summary',now(),'$this->author')";
				$this->query($sql);	
			}
		}
		
		public function edit_post(){
			$sql = "SELECT title FROM post WHERE title='$this->title' AND id != $this->id";
			$this->query($sql);
			if($this->num_row() > 0){
				return "fail";
			}else{
			$sql = "UPDATE post SET title='$this->title',content='$this->content',image='$this->image',cate_id='$this->cate_id',sub_cate='$this->sub_id',summary='$this->summary',date_post=now(),author='$this->author' WHERE id=$this->id";
			$this->query($sql);
			}
		}
		public function list_post(){
			$sql = "SELECT * FROM post ORDER BY id DESC";
			$this->query($sql);
			$data= array();
			$i=0;
			while($row = $this->fetch()){
$data[$i]=array("id"=>$row['id'],"title"=>$row['title'],"content"=>$row['content'],"image"=>$row['image'],"cate_id"=>$row['cate_id'],"sub_id"=>$row['sub_cate'],"summary"=>$row['summary'],"date_post"=>$row['date_post'],"author"=>$row['author']);
			$i++;	
			}
			return $data;
		}
		// in ra bài viết cho edit
		public function list_edit($id){
			$sql = "SELECT * FROM post WHERE id =$id";
			$this->query($sql);
		 	$data = $this->fetch();
			return $data;
		}
		
		
		public function num_post(){
			$sql = "SELECT * FROM post";
			$this->query($sql);
			$row = $this->num_row();
			return $row;	
		}
		// ham chuyen doi cate_id ve ten danh muc
		public function convert_cate($cate_id){
			$sql = "SELECT cate_name FROM category WHERE id=$cate_id";
			$this->query($sql);
			$data = $this->fetch();
			return $data;	
		}
		// ham chuyen doi sub_id ve ten sub menu
		public function convert_sub($sub_id){
			$sql = "SELECT menu_name FROM sub_menu WHERE id =$sub_id";
			$this->query($sql);
			$data = $this->fetch();
			return $data;	
		}
		
		public function delete_post(){
			$sql = "DELETE FROM post WHERE id= $this->id";	
			$this->query($sql);
		}
		
		public function get_cate(){
			$sql = "SELECT * FROM category";	
			$this->query($sql);
			$data = array();
			$i=0;
			while($row = $this->fetch()){
				$data[$i]= array("id"=>$row['id'],"cate_name"=>$row['cate_name']);
				$i++;
			}
			return $data;
		}
		
		public function select_sub(){
			$sql = "SELECT * FROM sub_menu WHERE cate_id = '$this->sub_id'";
			$this->query($sql);
			$data = array();
			$i=0;
			while($row = $this->fetch()){
				$data[$i]= array("id"=>$row['id'],"menu_name"=>$row['menu_name']);
				$i++;
			}
			return $data;
		}
		
		
		
		
		
	}
?>