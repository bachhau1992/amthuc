<script language="javascript">
	$(document).ready(function(e) {
        $('#right #content #cate').change(function(){
			id = $('#cate').val();
			$.ajax({
				url:'controller/post/ajax.php',
				type:"POST",
				data:'cate_id='+id,
				async:true,
				success: function(kq){
					$('#right #content #sub_menu').html(kq);
				}
				
				})
			
			return false;
			});
    });
</script>
<div id="right" >
	<div id="content">
        <fieldset>
            <legend><h3>Sửa bài viết</h3></legend>
        	<form action="index.php?controller=user&action=post&method=edit_post&id=<?php echo $id_edit ?>" method="post">
            	<table>
            		<tr>
                    	<td colspan="2"><?php if(isset($notifi['edit'])){echo $notifi['edit'];}?></td>
                    </tr>
                    <tr>
                    	<td>Tên bài viết:</td>
                    	<td><input type="text" name="title_edit" class="box" required="required" value="<?php echo $data_edit['title'] ?>"/></td>
                    </tr>
                    <tr>
                    	<td>Nội dung bài viết:</td>
                    	<td>
                        	<textarea name="content_edit" class="text_box" id="textarea" required="required"><?php echo $data_edit['content'] ?></textarea>
                        	 <script>CKEDITOR.replace('textarea');</script>
                        </td>
                    </tr>
                    <tr>
                    	<td>Hình minh họa:</td>
                    	<td>
                        	<input id="xFilePath" name="img_edit" type="text" size="60" class="box" required="required" value="<?php echo $data_edit['image'] ?>"/>
							<input type="button" value="Browse Server" onclick="BrowseServer();" />
                        </td>
                    </tr>
                    <tr>
                    	<td>Thuộc danh mục</td>
                    	<td>
                        	<select name="cate_edit" id="cate">
                            <?php foreach($data as $row){?>
                            	<option value="<?php echo $row['id'] ?>"><?php echo $row['cate_name'] ?></option>
                                <?php }?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                    	<td>Thuộc sub danh mục</td>
                    	<td>
                        	<select name="sub_menu_edit" id="sub_menu">
                           
                            	<option value="">Sub menu</option>
                              
                            </select>
                        </td>
                    </tr>
                    <tr>
                    	<td>Tóm tắt nội dung</td>
                        <td>
                        	<textarea name="summary_edit" class="text_box" id="summary" required="required"><?php echo $data_edit['summary'] ?></textarea>
                        	 <script>CKEDITOR.replace('summary');</script>
                        </td>
                    </tr>
                    <tr>
                    	<td>Tên tác giả:</td>
                    	<td><input type="text" name="author_edit" class="box" required="required" value="<?php echo $data_edit['author'] ?>"/></td>
                    </tr>
                    <tr>
                    	<td><input type="submit" name="ok_edit" id="submit" value="Sửa bài viết"/></td>
                    
                    </tr>
            	</table>
            </form>
        </fieldset>
    </div>
</div>