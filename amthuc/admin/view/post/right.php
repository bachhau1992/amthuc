<script language="javascript">
	$(document).ready(function(e) {
        $('#right #content #cate').change(function(){
			id = $('#cate').val();
			$.ajax({
				url:'controller/post/ajax.php',
				type:"POST",
				data:'cate_id='+id,
				async:true,
				success: function(kq){
					$('#right #content #sub_menu').html(kq);
				}
				
				})
			
			return false;
			});
    });
</script>
<div id="right" >
	<div id="content">
        <fieldset>
            <legend><h3>Thêm bài viết</h3></legend>
        	<form action="" method="post">
            	<table>
            		<tr>
                    	<td colspan="2"><?php if(isset($notifi['post'])){echo $notifi['post'];}?></td>
                    </tr>
                    <tr>
                    	<td>Tên bài viết:</td>
                    	<td><input type="text" name="title" class="box" required="required"/></td>
                    </tr>
                    <tr>
                    	<td>Nội dung bài viết:</td>
                    	<td>
                        	<textarea name="content" class="text_box" id="textarea" required="required"></textarea>
                        	 <script>CKEDITOR.replace('textarea');</script>
                        </td>
                    </tr>
                    <tr>
                    	<td>Hình minh họa:</td>
                    	<td>
                        	<input id="xFilePath" name="img" type="text" size="60" class="box" required="required"/>
							<input type="button" value="Browse Server" onclick="BrowseServer();" />
                        </td>
                    </tr>
                    <tr>
                    	<td>Thuộc danh mục</td>
                    	<td>
                        	<select name="cate" id="cate">
                            <?php foreach($data as $row){?>
                            	<option value="<?php echo $row['id'] ?>"><?php echo $row['cate_name'] ?></option>
                                <?php }?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                    	<td>Thuộc sub danh mục</td>
                    	<td>
                        	<select name="sub_menu" id="sub_menu">
                           
                            	<option value="">Sub menu</option>
                              
                            </select>
                        </td>
                    </tr>
                    <tr>
                    	<td>Tóm tắt nội dung</td>
                        <td>
                        	<textarea name="summary" class="text_box" id="summary" required="required"></textarea>
                        	 <script>CKEDITOR.replace('summary');</script>
                        </td>
                    </tr>
                    <tr>
                    	<td>Tên tác giả:</td>
                    	<td><input type="text" name="author" class="box" required="required"/></td>
                    </tr>
                    <tr>
                    	<td><input type="submit" name="ok" id="submit" value="Thêm bài viết"/></td>
                    
                    </tr>
            	</table>
            </form>
        </fieldset>
    </div>
</div>

