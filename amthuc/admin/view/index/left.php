<body>
<div id="top">
	<div id="top-left"><a href="#">Amthucviet.vn</a></div>
    <div id="top-right"><p>Xin chào  <span><?php if(isset($_SESSION['name'])){echo $_SESSION['name'];}?></span> </p><i class="fa fa-sign-out" aria-hidden="true"></i><a href="index.php?controller=user&action=logout">Logout</a></div>
</div>
<div style="clear:both"></div>
<!---End top---->
<div id="left">

	<div class="category">
    	<div class="heading">
    		<h4><a href="#danhmuc"><i class="fa fa-tachometer" aria-hidden="true"></i> Quản lý danh mục</a></h4>
    
    	</div>
		<div class="cate_content" id="danhmuc">
            <ul>
                <li><i class="fa fa-angle-right" aria-hidden="true"></i><a href="index.php?controller=user&action=category&method=add"> Thêm danh mục</a></li>
                <li><i class="fa fa-angle-right" aria-hidden="true"></i><a href="index.php?controller=user&action=category&method=list"> Xem danh mục</a></li>
            </ul>
    	</div>
    </div>
    
    <div class="category">
    	<div class="heading">
    	<h4><a href="#sub_menu"><i class="fa fa-qrcode" aria-hidden="true"></i> Quản lý sub menu</a></h4>
    
        </div>
        <div class="cate_content" id="sub_menu">
            <ul>
                <li><i class="fa fa-angle-right" aria-hidden="true"></i><a href="index.php?controller=user&action=sub_category&method=add"> Thêm sub menu</a></li>
                <li><i class="fa fa-angle-right" aria-hidden="true"></i><a href="index.php?controller=user&action=sub_category&method=list"> Xem sub menu</a></li>
            </ul>
        </div>
    </div>
    
    <div class="category">
    	<div class="heading">
    	<h4><a href="#menu"><i class="fa fa-industry" aria-hidden="true"></i> Quản lý menu bottom</a></h4>
    
    	</div>
        <div class="cate_content" id="menu">
            <ul>
                <li><i class="fa fa-angle-right" aria-hidden="true"></i><a href=""> Thêm menu</a></li>
                <li><i class="fa fa-angle-right" aria-hidden="true"></i><a href=""> Xem menu</a></li>
            </ul>
        </div>
    </div>
    <div class="category">
    	<div class="heading">
    	<h4><a href="#baiviet"><i class="fa fa-newspaper-o" aria-hidden="true"></i> Quản lý bài viết</a></h4>
    
        </div>
        <div class="cate_content" id="baiviet">
            <ul>
                <li><i class="fa fa-angle-right" aria-hidden="true"></i><a href="index.php?controller=user&action=post&method=add_post"> Thêm bài viết</a></li>
                <li><i class="fa fa-angle-right" aria-hidden="true"></i><a href="index.php?controller=user&action=post&method=list_post"> Xem bài viết</a></li>
            </ul>
        </div>
    </div>
    <div class="category">
    	<div class="heading">
    	<h4><a href="#slide"><i class="fa fa-sliders" aria-hidden="true"></i> Quản lý slide</a></h4>
    
        </div>
        <div class="cate_content" id="slide">
            <ul>
                <li><i class="fa fa-angle-right" aria-hidden="true"></i><a href=""> Thêm sldie</a></li>
                <li><i class="fa fa-angle-right" aria-hidden="true"></i><a href=""> Xem slide</a></li>
            </ul>
        </div>
    </div>
    <div class="category">
    	<div class="heading">
    	<h4><a href="#tab"><i class="fa fa-line-chart" aria-hidden="true"></i> Quản lý menu tab</a></h4>
    
        </div>
        <div class="cate_content" id="tab">
            <ul>
                <li><i class="fa fa-angle-right" aria-hidden="true"></i><a href=""> Thêm menu tab</a></li>
                <li><i class="fa fa-angle-right" aria-hidden="true"></i><a href=""> Xem menu tab</a></li>
            </ul>
        </div>
    </div>
     <div class="category">
     	<div class="heading">
    	<h4><a href="#user"><i class="fa fa-user" aria-hidden="true"></i> Quản lý thành viên</a></h4>
    
        </div>
        <div class="cate_content" id="user">
            <ul>
                <li><i class="fa fa-angle-right" aria-hidden="true"></i><a href="index.php?controller=user&action=user&method=register"> Thêm thành viên</a></li>
                <li><i class="fa fa-angle-right" aria-hidden="true"></i><a href="index.php?controller=user&action=user&method=list_user"> Xem thành viên</a></li>
            </ul>
        </div>
    </div>
</div>