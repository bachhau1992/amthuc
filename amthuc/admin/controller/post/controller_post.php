<?php 
	$post = new post();
	$data = $post->get_cate();
	$notifi = array();
	if(isset($_POST['ok'])){
		$title = $_POST['title'];
		$content = $_POST['content'];
		$image = $_POST['img'];
		$cate = $_POST['cate'];
		$sub_menu = $_POST['sub_menu'];
		$summary = $_POST['summary'];
		$author = $_POST['author'];
		$post->set_title($title);
		$post->set_content($content);
		$post->set_image($image);
		$post->set_cate_id($cate);
		$post->set_sub_id($sub_menu);
		$post->set_summary($summary);
		$post->set_author($author);
		if($post->add_post()== "fail"){
			$notifi['post'] = "Tên bài viết bị trùng!";
		}else{
			$notifi['post'] = "Đã thêm bài viết!";	
		}
		
		
	}
	// in danh sách bài viết
	$data_list = $post->list_post();
	// sửa bài viết
	if(isset($_GET['id'])){
		$id_edit = $_GET['id'];
		$data_edit = $post->list_edit($id_edit);
		if(isset($_POST['ok_edit'])){
			$title_edit = $_POST['title_edit'];
			$content_edit = $_POST['content_edit'];
			$img_edit = $_POST['img_edit'];
			$cate_edit = $_POST['cate_edit'];
			$sub_menu_edit = $_POST['sub_menu_edit'];
			$summary_edit = $_POST['summary_edit'];
			$author_edit = $_POST['author_edit'];
			$post->set_id($id_edit);
			$post->set_title($title_edit);
			$post->set_content($content_edit);
			$post->set_image($img_edit);
			$post->set_cate_id($cate_edit);
			$post->set_sub_id($sub_menu_edit);
			$post->set_summary($summary_edit);
			$post->set_author($author_edit);
			if($post->edit_post() == "ok"){
				$notifi['edit'] = "Ten bai viet bi trung";
			}else{
				header("Location: index.php?controller=user&action=post&method=list_post");	
			}
		}
	}
	// Xoa bai viet 
	if(isset($_GET['id_delete'])){
		$id_delete = $_GET['id_delete'];
		$post->set_id($id_delete);
		$post->delete_post();
		header("Location: index.php?controller=user&action=post&method=list_post");
	}
	
	

if(isset($_GET['method'])){
	include('view/post.php'); 
}

?>