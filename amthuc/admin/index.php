<?php 
	ob_start();
	session_start();
	include("../library/database.php");
	include("model/autoload.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Quan tri he thong</title>
<script language="javascript" src="../public/js/jquery.min.js"></script>
<script src="../public/ckeditor/ckeditor.js" language="javascript"></script>
<script src="../public/ckfinder/ckfinder.js" language="javascript"></script>
<script type="text/javascript">

function BrowseServer()
{
	var finder = new CKFinder();
	finder.basePath = '/amthuc/public/img/';	// The path for the installation of CKFinder (default = "/ckfinder/").
	finder.selectActionFunction = SetFileField;
	finder.popup();

	
}
function SetFileField( fileUrl )
{
	document.getElementById( 'xFilePath' ).value = fileUrl;
}
	</script>
</head>

<body>
<?php 
	
	if(isset($_GET['controller'])){
		include('controller/controller.php');
	}else{
		header("Location: index.php?controller=user&action=login");
		if(isset($_SESSION['name'])){
		header("Location: index.php?controller=user&action=index");	
		}
	}
	

?>
</body>
</html>