<?php
class database {
	protected $server = "localhost";
	protected $user = "admin";
	protected $pass = "123456";
	protected $db = "am_thuc";
	protected $conn = NULL;
	protected $result = NULL;

	function __construct() {
		$this->conn = mysqli_connect($this->server, $this->user, $this->pass);
		if (mysqli_connect_error()) {
			echo "Khong ket noi duoc" . mysqli_connect_error();
		}
		$select = mysqli_select_db($this->conn, $this->db);
		if (!$select) {
			echo "khong ket noi duoc database" . mysqli_connect_error();
		}
	}

	public function query($sql) {
		$this->free_query();
		$this->result = mysqli_query($this->conn, $sql);
	}
	public function free_query() {
		if ($this->result) {
			mysqli_free_result($this->result);
		}
	}

	public function num_row() {
		if ($this->result) {
			$row = mysqli_num_rows($this->result);
			return $row;
		}
	}

	public function fetch() {
		if ($this->result) {
			$fetch = mysqli_fetch_assoc($this->result);
			return $fetch;
		}
	}

	public function close() {
		if ($this->conn) {
			mysqli_close($this->conn);
		}
	}
}
?>