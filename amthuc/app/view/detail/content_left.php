<!-- Content Main -->
<div id="main-content">
	<div class="detail">
    <div class="direct"><a href="<?php echo base_url(); ?>">Trang chủ</a><span> >> </span>
    <?php $title_data_cate = convert_key($data_cate_dt['cate_name'])?>
    <a href="<?php echo base_url(); ?>chuyen-muc/<?php echo $data_detail['cate_id'] ?>/<?php echo $title_data_cate ?>.html"
    title="<?php echo $data_cate_dt['cate_name'] ?>"><?php echo $data_cate_dt['cate_name'] ?></a>
    <?php if($data_sub_dt['menu_name'] != ""){?>
		<span> >> </span>
        <?php $title_data_sub = convert_key($data_sub_dt['menu_name'])?>
        <a href="<?php echo base_url(); ?>chuyen-muc/<?php echo $data_detail['cate_id'] ?>/<?php echo $data_detail['sub_cate'] ?>/<?php echo $title_data_cate ?>/<?php echo $title_data_sub ?>.html" title="<?php echo $data_sub_dt['menu_name'];?>"><?php echo $data_sub_dt['menu_name'];?></a>
	<?php }else{
	echo "";	
	}
	?>
    </div>
    
       <div class="left"> 
    	<div class="detail-left">
        <?php 
		// convert date 
		$time = strtotime($data_detail['date_post']);
		$date = date('d/m/Y',$time);
		?>
        	<h2><?php echo $data_detail['title'] ?></h2><span class="date"><?php echo $date ?></span>
        	<div class="summary"> <?php echo $data_detail['summary'] ?></div>
            <p class="relate"><a  href="#">Nem cua bể phong cách miền Nam cho Tất niên</a></p>
            <div class="main-detail">
            	<?php echo $data_detail['content']; ?>
            </div>
            <div class="author"><?php echo $data_detail['author']; ?></div>
            <div style="clear:both"></div>
            
            <?php foreach($data_relate as $relate){ ?>
            <?php $title_relate = convert_key($relate['title']);?>
            <p class="relate"><a  href="<?php echo base_url(); ?>bai-viet/<?php echo $relate['id'] ?>/<?php echo $title_relate ?>.html"
            title="<?php echo $relate['title'] ?>"><?php echo $relate['title'] ?></a></p>
            <?php }?>
            
        </div>
     </div>
        <!--end detail left-->