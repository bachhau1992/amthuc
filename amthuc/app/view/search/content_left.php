<!--- content --->
<div id="wrapper">
	<div id="content">
    	<div id="direct"><a href="<?php echo base_url(); ?>">Trang chủ</a><span>>></span><a href="<?php echo base_url(); ?>search.html">Tìm kiếm</a></div>
		<div class="left">
        	<div class="content-left">
            <?php if(isset($notifi['search'])){?>
            	<h3 style="font-family:Arial, Helvetica, sans-serif; color:#666;"><?php echo $notifi['search'] ?></h3>
            <?php }else{?>
   <h3 style="font-family:Arial, Helvetica, sans-serif; color:#666;">Kết quả tìm kiếm với từ khóa <span style="color:red;"><?php echo $key ?></span> :</h3>
        		<ul id="list">
                <?php foreach($data_search as $row_search){?>
                <?php 
					$time = strtotime($row_search['date']);
					$date = date('d/m/Y',$time);
					$summary_search = substr($row_search['summary'],0,200);
				?>
                	<li>
                    	<img src="<?php echo $row_search['image'] ?>" alt="<?php echo $row_search['title'] ?>" />
                    	<div class="cate-left">
                        	<?php $title_search = convert_key($row_search['title'])?>
                        	<h3><a href="<?php echo base_url(); ?>bai-viet/<?php echo $row_search['id'] ?>/<?php echo $title_search ?>.html" 
                            title="<?php echo $row_search['title'] ?>"><?php echo $row_search['title'] ?></a></h3>
                            <span><?php echo $date ?></span>
                        	<p><?php echo $summary_search ?>....</p>
                        </div>
                    </li>
                <?php }?>
                	<!-- end notifi search -->
                   <?php }?> 
                </ul>
        	</div>