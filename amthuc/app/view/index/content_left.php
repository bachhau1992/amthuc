 <div id="content">
    <div class="left">	
        <div class="content-left">
        
    		<h3><span>Ẩm thực Việt</span></h3>
            
            <div class="news_left">
            	<img src="<?php echo $data_one_vn['image'] ?>" width="317px" height="217px"/>
                
                <?php $title_vn = convert_key($data_one_vn['title'])?>
                <a href="<?php echo base_url(); ?>bai-viet/<?php echo $data_one_vn['id'] ?>/<?php echo $title_vn ?>.html" title="<?php echo $data_one_vn['title'] ?>"><?php echo $data_one_vn['title'] ?></a>
            	<p><?php echo $data_one_vn['summary'] ?></p>
                
                <br/>
                <ul>
                <?php foreach($relate_vn as $row_vn){?>
                <?php $title_relate_vn = convert_key($row_vn['title'])?>
                <li><a href="<?php echo base_url(); ?>bai-viet/<?php echo $row_vn['id'] ?>/<?php echo $title_relate_vn ?>.html" title="
				<?php echo $row_vn['title'] ?>"><?php echo $row_vn['title'] ?> </a></li>
                <?php }?>
                </ul>
                
               
            </div>
            <div class="news_right">
            	<ul>
                <?php foreach($data_cate_vn as $key){?>
                <?php $summary = substr($key['summary'],0,100);?>
                <?php $key_title = convert_key($key['title'])?>
            		<li>
                    	<a href="<?php echo base_url(); ?>bai-viet/<?php echo $key['id'] ?>/<?php echo $key_title ?>.html">
                        <img src="<?php echo $key['image'] ?>" width="80px" height="58px" /> </a>
                        <div>
                        	<a href="<?php echo base_url(); ?>bai-viet/<?php echo $key['id'] ?>/<?php echo $key_title ?>.html">
							<?php echo $key['title'] ?></a>
                        	<p><?php echo $summary; ?>...
                			</p>
                      
                    	</div>
                    </li>
                    <?php }?>
            	</ul>
            </div>
   		</div>
    	<!-- end content left-->
         <!-- Content left second --> 
        <div class="content-left">
    		<h3><span>Ẩm thực Hàn Quốc </span></h3>
           
            <div class="news_left">
            	<?php $title_one_hq = convert_key($data_one_hq['title'])?>
            	<img src="<?php echo $data_one_hq['image'] ?>" width="317px" height="217px"/>
                <a href="<?php echo base_url(); ?>bai-viet/<?php echo $data_one_hq['id'] ?>/<?php echo $title_one_hq ?>.html" 
                title="<?php echo $data_one_hq['title'] ?>"><?php echo $data_one_hq['title'] ?></a>
            	<p><?php echo $data_one_hq['summary'] ?></p>
                
                <br/>
                <ul>
                <?php foreach($relate_hq as $row_hq){?>
                <?php $title_relate_hq = convert_key($row_hq['title'])?>
                <li><a href="<?php echo base_url(); ?>bai-viet/<?php echo $row_hq['id'] ?>/<?php echo $title_relate_hq ?>.html"
                title="<?php echo $row_hq['title'] ?> "><?php echo $row_hq['title'] ?> </a></li>
                <?php }?>
                </ul>
                
            </div>
            
            <div class="news_right">
            	<ul>
                <?php foreach($data_sub_hq as $value){?>
                <?php $value_title = convert_key($value['title'])?>
                <?php $summary_hq = substr($value['summary'],0,100);?>
            		<li>
                    	<a href="http://amthuc123.esy.es/bai-viet/<?php echo $value['id'] ?>/<?php echo $value_title ?>.html"><img src="<?php echo $value['image'] ?>" width="80px" height="58px" /> </a>
                        <div>
                        	<a href="http://amthuc123.esy.es/bai-viet/<?php echo $value['id'] ?>/<?php echo $value_title ?>.html"
                            title="<?php echo $value['title'] ?>"><?php echo $value['title'] ?></a>
                        	<p><?php echo $summary_hq ?> ...
                			</p>
                      
                    	</div>
                    </li>
            <?php }?>
            		
                    
            	</ul>
            </div>
   		</div>
        <!-- End Content left second-->
         <!-- Content left thirt-->
        <div class="content-left">
    		<h3><span>Công thức nấu ăn </span></h3>
           
            <div class="news_left">
            	<?php $title_one_ct = convert_key($data_one_ct['title']);?>
            	<img src="<?php echo $data_one_ct['image'] ?>" width="317px" height="217px"/>
                <a href="http://amthuc123.esy.es/bai-viet/<?php echo $data_one_ct['id'] ?>/<?php echo $title_one_ct ?>.html"
                title="<?php echo $data_one_ct['title'] ?>"><?php echo $data_one_ct['title'] ?></a>
            	<p><?php echo $data_one_ct['summary'] ?> </p>
                
                <br/>
                <ul>
                 <?php foreach($relate_ct as $row_ct){?>
                 <?php $row_ct_title = convert_key($row_ct['title']); ?>
                <li><a href="http://amthuc123.esy.es/bai-viet/<?php echo $row_ct['id'] ?>/<?php echo $row_ct_title ?>.html"
                title="<?php echo $row_ct['title'] ?>"><?php echo $row_ct['title'] ?> </a></li>
                <?php }?>
                </ul>
                
            </div>
           
            
            <div class="news_right">
            	<ul>
                <?php foreach($data_sub_ct as $sub_ct){?>
                <?php $sub_ct_title = convert_key($sub_ct['title'])?>
                <?php $summary_ct = substr($sub_ct['summary'],0,100);?>
            		<li>
                    	<a href="http://amthuc123.esy.es/bai-viet/<?php echo $sub_ct['id'] ?>/<?php echo $sub_ct_title ?>.html"
                        ><img src="<?php echo $sub_ct['image'] ?>" width="80px" height="58px" /> </a>
                        <div>
                        	<a href="http://amthuc123.esy.es/bai-viet/<?php echo $sub_ct['id'] ?>/<?php echo $sub_ct_title ?>.html"
                            title="$sub_ct['title']"><?php echo $sub_ct['title'] ?></a>
                        	<p><?php echo $summary_ct ?> ...
                			</p>
                      
                    	</div>
                    </li>
            	<?php } ?>
            	</ul>
            </div>
   		</div>
        <!-- End Content left thirt-->
     </div>
     <!--End left-->