<!-- Content Main -->
<div id="main-content">
<!-- Slide bar-->
	<div id="slide-bar">
    	<div id="slide" >
		  	<ul class="slide-content">
	        	<li class=" slide-item ui-tabs-selected" id="nav-fragment-1">
            		<a href="#fragment-1">
                	<img src="<?php echo base_url(); ?>public/image/image1-small.jpg" alt="" />
                	<span>Cách nấu canh đậu tương lên men ngon như người Hàn </span>
                 	</a>
           		</li>
	        	<li class="slide-item" id="nav-fragment-2">
            		<a href="#fragment-2">
                	<img src="<?php echo base_url(); ?>public/image/image2-small.jpg" alt="" />
                    <span>Cách làm bánh Tteokbokki ngon chuẩn vị Hàn Quốc</span>
            		</a>
            	</li>
            
           		<li class="slide-item" id="nav-fragment-2">
            		<a href="#fragment-3">
                	<img src="<?php echo base_url(); ?>public/image/image3-small.jpg" alt="" />
                    <span>Cách làm kim chi kho thịt cực ngon, bạn đã thử chưa?</span>
            		</a>
            	</li>
            
	        	<li class="slide-item" id="nav-fragment-2">
            		<a href="#fragment-4">
                	<img src="<?php echo base_url(); ?>public/image/image4-small.jpg" alt="" />
                    <span>Cách làm salad khoai tây kiểu Hàn thơm ngon ngất ngây</span>
            		</a>
            	</li>
            
	      	</ul>

	    <!-- First Content -->
	    <div id="fragment-1" class="ui-tabs-panel" >
			<img src="<?php echo base_url(); ?>public/image/image1.jpg" width="590px" height="400px;" alt="" />
			 <div class="info" >
				<h2><a href="#" >Cách nấu canh đậu tương lên men ngon như người Hàn</a></h2>
				<p>Canh đậu tương lên men là món ăn truyền thống của người Hàn Quốc, với hương vị hấp dẫn và độc đáo. 
                	Nếu bạn là người yêu thích những bộ phim của xứ sở kim chi không thể không biết tới món ăn này. Bài viết
                 	dưới đây sẽ chia sẻ về cách nấu canh đậu tương lên men chuẩn vị Hàn Quốc. Cách nấu canh đậu tương lên men 
                	Để thực hiện cách nấu canh đậu...<a href="#" > read more</a></p>
			 </div>
	    </div>

	    <!-- Second Content -->
	    <div id="fragment-2" class="ui-tabs-panel ui-tabs-hide" >
			<img src="<?php echo base_url(); ?>public/image/image2.jpg" width="590px" height="400px;" alt="" />
			 <div class="info" >
				<h2><a href="#" >Cách làm bánh Tteokbokki ngon chuẩn vị Hàn Quốc</a></h2>
				<p>Tteokbokki là món bánh gạo cay nổi tiếng ở Hàn Quốc, khi du nhập vào nước ta được nhiều người yêu thích 
                bởi hương vị cay đặc trưng của xứ sở kim chi. Thay vì thưởng thức món bánh này ngoài hàng, các bạn hãy cùng 
                học cách làm bánh Tteokbokki cực ngon dưới đây nhé. Cách làm bánh Tteokbokki Hướng dẫn cách làm bánh Tteokbokki 
                đơn giản,.....
                    <a href="#" >read more</a>
                </p>
			 </div>
	    </div>

	    <!-- Third Content -->
       <div id="fragment-3" class="ui-tabs-panel ui-tabs-hide" >
			<img src="<?php echo base_url(); ?>public/image/image3.jpg" width="590px" height="400px;" alt="" />
			 <div class="info" >
				<h2><a href="#" >Cách làm kim chi kho thịt cực ngon, bạn đã thử chưa?</a></h2>
				<p>Bạn là người yêu thích những món ăn được chế biến từ kim chi, vậy hãy cùng khám phá cách làm kim chi kho thịt ngon, 
                béo ngậy để chiêu đãi cả gia đình cùng thưởng thức nhé. Vị cay cay của kim chi hòa quyện cùng với vị béo ngậy của thịt
                 ba chi tạo nên hương vị tuyệt vời. Cùng tham khảo bài viết dưới đây để chi tiết cách làm món ăn này nhé.....
                    <a href="#" >read more</a>
                </p>
			 </div>
	    </div>
        
        
        <!-- four Content -->
        
        <div id="fragment-4" class="ui-tabs-panel ui-tabs-hide" >
			<img src="<?php echo base_url(); ?>public/image/image4.jpg" width="590px" height="400px;" alt="" />
			 <div class="info" >
				<h2><a href="#" >Cách làm salad khoai tây kiểu Hàn thơm ngon ngất ngây</a></h2>
				<p>Salad khoai tây kiểu Hàn là món khai vị cực ngon rất được ưa chuộng. Hôm nay, cachlam9.com sẽ hướng dẫn các 
                bạn cách làm salad khoai tây kiểu Hàn đơn giản và tuyệt ngon nhé. Cùng theo dõi bài viết dưới đây để được hướng 
                dẫn cách làm chi tiết món ăn này nhé. Cách làm salad khoai tây kiểu Hàn Hướng dẫn cách làm salad khoai tây kiểu Hàn .....
                    <a href="#" >read more</a>
                </p>
			 </div>
	    </div>
        <!-- four Content -->
		</div>
	</div>
            
        
 <!-- end slide bar-->