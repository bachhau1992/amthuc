<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Ẩm thực Việt</title>
<link rel="shortcut icon" href="<?php echo base_url(); ?>public/image/favicon.png" />
<link href="<?php echo base_url(); ?>public/css/style.css" type="text/css" rel="stylesheet" />
<script src="http://code.jquery.com/jquery-1.8.2.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>public/js/jquery-ui.min.js" language="javascript"></script>
<script language="javascript" type="text/javascript" src="<?php echo base_url(); ?>public/js/javascript.js"></script>
<script language="javascript" src="<?php echo base_url(); ?>public/js/jquery.carouFredSel-6.0.4-packed.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$("#slide > ul").tabs({fx:{opacity: "toggle"}}).tabs("rotate", 5000, true);
	});
</script>
<script type="text/javascript">
			$(function() {

				$('#carousel ul').carouFredSel({
					prev: '#prev',
					next: '#next',
					pagination: "#pager",
					scroll: 1000
				});
	
			});
		</script>
</head>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.8";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
