<?php 
	class post extends database{
		public function get_post_cate($cate_id){
			$sql = "SELECT * FROM post WHERE cate_id=$cate_id LIMIT 1,4";
			$this->query($sql);
			$data = array();
			$i=0;
			while($row = $this->fetch()){
			$data[$i]= array("id"=>$row['id'],"title"=>$row['title'],"content"=>$row['content'],"image"=>$row['image'],"cate_id"=>$row['cate_id'],"sub_id"=>$row['sub_cate'],"summary"=>$row['summary'],"date"=>$row['date_post'],"author"=>$row['author']);
			$i++;
			}
			return $data;
		}
		
		public function get_post_sub($sub_id){
			$sql = "SELECT * FROM post WHERE sub_cate=$sub_id LIMIT 1,4";
			$this->query($sql);
			$data = array();
			$i=0;
			while($row = $this->fetch()){
			$data[$i]= array("id"=>$row['id'],"title"=>$row['title'],"content"=>$row['content'],"image"=>$row['image'],"cate_id"=>$row['cate_id'],"sub_id"=>$row['sub_cate'],"summary"=>$row['summary'],"date"=>$row['date_post'],"author"=>$row['author']);
			$i++;
			}
			return $data;
			
		}
		
		public function get_one_cate($cate_id){
			$sql = "SELECT * FROM post WHERE cate_id=$cate_id LIMIT 0,1";
			$this->query($sql);
			$data =$this->fetch();
			return $data;
		}
		
		public function get_one_sub($sub_id){
			$sql = "SELECT * FROM post WHERE sub_cate=$sub_id LIMIT 0,1";
			$this->query($sql);
			$data = $this->fetch();
			return $data;
		}
		// relate of post flow cate menu
		public function relate($cate_id,$id){
			$sql = "SELECT id,title FROM post WHERE cate_id=$cate_id AND id !=$id ORDER BY RAND() LIMIT 4";
			$this->query($sql);
			$data = array();
			$i=0;
			while($row = $this->fetch()){
				$data[$i] = array("id"=>$row['id'],"title"=>$row['title']);
				$i++;
			}
			return $data;
		}
		// relate of post flow sub menu
		public function relate_sub($sub_id,$id){
			$sql = "SELECT id,title FROM post WHERE sub_cate=$sub_id AND id !=$id ORDER BY RAND() LIMIT 4";
			$this->query($sql);
			$data = array();
			$i=0;
			while($row = $this->fetch()){
				$data[$i] = array("id"=>$row['id'],"title"=>$row['title']);
				$i++;
			}
			return $data;
		}
		
		
		

	}
?>