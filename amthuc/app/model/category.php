<?php 
	class category extends database{
	// get sub menu
	public function get_sub_menu($sub_id){
		$sql = "SELECT * FROM sub_menu WHERE id=$sub_id";		
		$this->query($sql);	
		$data = $this->fetch();
		return $data;
	}	
	// get cate menu
	public function get_cate_menu($cate_id){
		$sql = "SELECT * FROM category WHERE id=$cate_id";		
		$this->query($sql);	
		$data = $this->fetch();
		return $data;
	}	
	// phan trang theo category
	// get total record
	public function get_num_page($cate_id){
		$sql = "SELECT * FROM post WHERE cate_id =$cate_id";
		$this->query($sql);
		$num_record = $this->num_row();
		// so record trong 1 trang la bang 7
		$num_page = ceil($num_record/7);
		return $num_page;	
	}
	
		// lay cac post theo cate va sub menu
	public function get_post_cate($cate_id,$page){
		$position = ($page - 1)*7;
		$sql_post = "SELECT * FROM post WHERE cate_id =$cate_id LIMIT $position,7";
		$this->query($sql_post);
		$data = array();
		$i=0;
		while($row = $this->fetch()){
			$data[$i]= array("id"=>$row['id'],"title"=>$row['title'],"content"=>$row['content'],"image"=>$row['image'],"cate_id"=>$row['cate_id'],"sub_id"=>$row['sub_cate'],"summary"=>$row['summary'],"date"=>$row['date_post'],"author"=>$row['author']);
			$i++;
		}
		return $data;
	}
	// get num page theo sub menu
	public function get_num_sub($sub_id){
		$sql = "SELECT * FROM post WHERE sub_cate = $sub_id";
		$this->query($sql);
		$num_record = $this->num_row();
		// lay 7 record trong 1 page
		$num_page = ceil($num_record/7);
		return $num_page;
	}
	//get post flow sub menu
	public function get_post_sub($sub_id,$page){
		$position = ($page - 1)*7;
		$sql = "SELECT * FROM post WHERE sub_cate =$sub_id LIMIT $position,7";
		$this->query($sql);
		$data = array();
		$i=0;
		while($row = $this->fetch()){
			$data[$i]= array("id"=>$row['id'],"title"=>$row['title'],"content"=>$row['content'],"image"=>$row['image'],"cate_id"=>$row['cate_id'],"sub_id"=>$row['sub_cate'],"summary"=>$row['summary'],"date"=>$row['date_post'],"author"=>$row['author']);
			$i++;
		}
		return $data;
	}
	//get tip 
	public function tip_cate(){
			$sql = "SELECT * FROM post WHERE sub_cate =5 LIMIT 4";
			$this->query($sql);
			$data = array();
			$i=0;
			while($row = $this->fetch()){
			$data[$i]= array("id"=>$row['id'],"title"=>$row['title'],"content"=>$row['content'],"image"=>$row['image'],"cate_id"=>$row['cate_id'],"sub_id"=>$row['sub_cate'],"summary"=>$row['summary'],"date"=>$row['date_post'],"author"=>$row['author']);
			$i++;
			}
			return $data;
		}



	}
?>