<?php 
	class detail extends database{
		public function get_detail($id){
			$sql = "SELECT * FROM post WHERE id =$id";	
			$this->query($sql);
			$data = $this->fetch();
			return $data;
		}

		public function get_cate_menu($cate_menu){
			$sql = "SELECT * FROM category WHERE id=$cate_menu";
			$this->query($sql);
			$data = $this->fetch();
			return $data;
		}

		public function get_sub_menu($sub_menu){
			$sql = "SELECT * FROM sub_menu WHERE id=$sub_menu";
			$this->query($sql);
			$data = $this->fetch();
			return $data;
		}
		
		public function relate($cate_menu,$id){
			$sql = "SELECT id,title FROM post WHERE cate_id=$cate_menu AND id !=$id LIMIT 0,4";
			$this->query($sql);
			$data = array();
			$i=0;
			while($row = $this->fetch()){
				$data[$i] = array("id"=>$row['id'],"title"=>$row['title']);
				$i++;
			}
			return $data;
		}

		//get tip 
		public function tip($detail_id){
			$sql = "SELECT * FROM post WHERE sub_cate =5 AND id!=$detail_id ORDER BY RAND() LIMIT 7";
			$this->query($sql);
			$data = array();
			$i=0;
			while($row = $this->fetch()){
			$data[$i]= array("id"=>$row['id'],"title"=>$row['title'],"content"=>$row['content'],"image"=>$row['image'],"cate_id"=>$row['cate_id'],"sub_id"=>$row['sub_cate'],"summary"=>$row['summary'],"date"=>$row['date_post'],"author"=>$row['author']);
			$i++;
			}
			return $data;
		}


	}
?>