<?php 
	if(isset($_GET['action'])){
		switch($_GET['action']){
			case "index": include('app/controller/index/controller_index.php');
			break;
			case "detail": include('app/controller/detail/controller_detail.php');
			break;
			case "category": include('app/controller/category/controller_category.php');
			break;
			case "search":
			if(isset($_POST['search'])){
				include('app/controller/search/controller_search.php');
			}else{
				header("Location: index.php");
			}
			break;
			case "404": include('app/controller/404/controller_404.php');
		}
	}


?>